import React, { Component } from 'react'
import TechItem from './TechItem'
class Techlist extends Component {
    //estado
    state = {
        newTech: '',
        techs: [
            'Node.js',
            'React.js',
            'React Native'
        ]
    };
    //metodo para o bottao do input
    handleInputChange = e => {
        this.setState({ newTech: e.target.value })
    }
    //
    handleSubmit = e =>{
        e.preventDefault()

        this.setState({ techs : [... this.state.techs, this.state.newTech ], newTech: '' })

    }

    handleDelete = (tech) => {
        this.setState({ techs: this.state.techs.filter(t => t != tech) })
    }

    render() {
         
        return (
            <form onSubmit={this.handleSubmit}>
                <h1>{this.state.newTech}</h1>
                <ul>
                    <TechItem tech='Express.js'/>
                    {this.state.techs.map(tech => <TechItem key={tech} tech={tech} onDelete={() => this.handleDelete(tech)}/>)}
                </ul>
                <input 
                    type="text" 
                    onChange={this.handleInputChange}
                    value = {this.state.newTech}
                />
                <button type="submit">Enviar</button>
            </form>
        )
    }
}

export default Techlist